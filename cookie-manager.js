let d = new Date( Date.now() + 900000 );

function setCookie(name, data) {
    return document.cookie = `${name}=${data};` + 'expires=' + d.toUTCString();
 };

function getCookie(name) {
    const yourCookie = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (yourCookie) return yourCookie[2];
}

function clearCookie(name) {
    const yourCookie = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (yourCookie) return document.cookie = `${name}=${yourCookie[2]}; max-age=0`;
}